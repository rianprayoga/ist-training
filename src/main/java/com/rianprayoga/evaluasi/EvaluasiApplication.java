package com.rianprayoga.evaluasi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvaluasiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvaluasiApplication.class, args);
	}

}
